package com.application.chat

import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@RestController
class ChatController {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    }

    @RequestMapping(value = ["/chat/{topic}"],
            method = [RequestMethod.POST],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun doPost(@PathVariable("topic") topic: String, @RequestBody question: String): String {
        val gson = Gson()
        logger.info("Incoming request body with chat question $question")
        val input = question.split("=")
        val question = QuestionHandler(input[1].replace("+", " "))
        val chatService: ChatService = ChatServiceImpl()
        val response = chatService.getResponseForQuestion(question)
        return gson.toJson(response)
    }
}

