package com.application.chat

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.servlet.ModelAndView

@Controller
class ViewController {

    @RequestMapping(value = ["/views/{topic}"], method = [RequestMethod.GET])
    fun getIndex(@PathVariable("topic") topic: String): ModelAndView {
        val topicMap = hashMapOf("topic" to topic)
        return ModelAndView("index", topicMap)
    }
}