package com.application.chat

data class QuestionHandler(var question: String)

data class Responses(
        val responseList: ArrayList<Response>
)

data class Response(
        val input: List<String?>? = null,
        val answer: String? = null
)

object DefaultAnswers {
    val cantAnswer = "I'm sorry I cannot answer that, please rephrase your question"
}