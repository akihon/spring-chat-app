package com.application.chat

import com.google.gson.Gson
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.charset.Charset

interface ResponseEngine {
    fun getResponseForInput(input: String): String
}

class ResponseEngineImpl : ResponseEngine {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    override fun getResponseForInput(input: String): String {
        logger.info("Attempting to get the correct response for the input $input")

        val jsonString = this::class.java.classLoader.getResource("responses.json").readText(Charset.defaultCharset())

       val gson = Gson()
        val responses = gson.fromJson(jsonString, Responses::class.java)

        val answer = responses.responseList.forEach {
            val inputList = it.input
            if(inputList!!.contains(input))  {
                return it.answer ?: DefaultAnswers.cantAnswer
            }
        }

        return answer.toString()
    }
}