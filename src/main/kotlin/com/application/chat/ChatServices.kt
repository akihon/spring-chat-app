package com.application.chat

import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface ChatService {
    fun getResponseForQuestion(question: QuestionHandler): String
}

class ChatServiceImpl : ChatService {

    companion object {
        val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    }

    override fun getResponseForQuestion(question: QuestionHandler): String {
        logger.info("Getting the response for question input ${question.question}")
        val responseEngine: ResponseEngine = ResponseEngineImpl()
        return responseEngine.getResponseForInput(question.question)
    }
}