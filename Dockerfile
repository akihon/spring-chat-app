FROM maven:3.5-jdk-8 as maven
RUN ls -al /usr/share/maven

FROM oracle/graalvm-ce as builder
ENV GRAALVM_HOME=${JAVA_HOME}
COPY --from=maven /usr/share/maven /usr/share/maven
ENV MVN_HOME=/usr/share/maven
ENV M2_HOME=${MVN_HOME}
ENV PATH=${JAVA_HOME}/bin:${GRAALVM_HOME}/bin:${MVN_HOME}/bin:{$M2_HOME}/bin:$PATH
RUN gu install native-image

RUN mkdir ./spring-chat-application
ADD . ./spring-chat-application

RUN cd ./spring-chat-application && mvn clean install

RUN cd ./spring-chat-application && native-image -jar ./target/spring-chat-application-*.jar --no-server \
       -H:EnableURLProtocols=http \
       -H:Name=chat \
       -H:+ReportUnsupportedElementsAtRuntime \
       -H:+AllowVMInspection

FROM frolvlad/alpine-glibc
EXPOSE 8000
COPY --from=builder ./spring-chat-application/chat ./chat
ENTRYPOINT ["./chat"]